package de.xearox.textfilechanger;

import java.awt.EventQueue;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JTextPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.NumberFormatter;

import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.Scanner;
import java.awt.event.ActionEvent;
import javax.swing.JSpinner;
import javax.swing.JTextField;

public class TextFileChanger {

	private JFrame frame;
	private File sourceFile = null;
	private File outputFile = null;
	
	private JLabel inputFileLabel = new JLabel("...");
	private JLabel outputFileLabel = new JLabel("...");
	private JSpinner spinner = new JSpinner();
	private final JLabel lblOutputFormat = new JLabel("Output Format | %value% for replacement");
	private JTextField txttimevaluedataspawnobjup;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TextFileChanger window = new TextFileChanger();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TextFileChanger() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 656, 241);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		inputFileLabel.setBounds(147, 15, 428, 14);
		frame.getContentPane().add(inputFileLabel);
		
		outputFileLabel.setBounds(147, 53, 428, 14);
		frame.getContentPane().add(outputFileLabel);
		
		JButton btnAddInputFile = new JButton("Add Input File");
		btnAddInputFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
//			    FileNameExtensionFilter filter = new FileNameExtensionFilter("txt");
//			    chooser.setFileFilter(filter);
			    int returnVal = chooser.showOpenDialog(null);
			    if(returnVal == JFileChooser.APPROVE_OPTION) {
			    	sourceFile = chooser.getSelectedFile();
			    	inputFileLabel.setText(sourceFile.getAbsolutePath());
			    }
			}
		});
		btnAddInputFile.setBounds(10, 11, 127, 23);
		frame.getContentPane().add(btnAddInputFile);
		
		JButton btnNewButton = new JButton("Set Output File");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
//			    FileNameExtensionFilter filter = new FileNameExtensionFilter("txt");
//			    chooser.setFileFilter(filter);
			    int returnVal = chooser.showSaveDialog(null);
			    if(returnVal == JFileChooser.APPROVE_OPTION) {
			    	outputFile = chooser.getSelectedFile();
			    	outputFileLabel.setText(outputFile.getAbsolutePath());
			    }
			}
		});
		btnNewButton.setBounds(10, 49, 127, 23);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnRun = new JButton("Run");
		btnRun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(sourceFile == null) {
					JOptionPane.showMessageDialog(frame,"You haven't select any source file.","No source file selected...",JOptionPane.ERROR_MESSAGE);
					return;
				}
				if(outputFile == null) {
					JOptionPane.showMessageDialog(frame,"You haven't select any output file.","No output file selected...",JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				try {
					FileReader fr = new FileReader(sourceFile);
					BufferedReader br = new BufferedReader(fr);
					Scanner scanner = new Scanner(br);
					
					FileWriter fw = new FileWriter(outputFile);
					BufferedWriter bw = new BufferedWriter(fw);
					
					DecimalFormatSymbols sym = new DecimalFormatSymbols(new Locale("de", "DE"));
					DecimalFormat df = new DecimalFormat("##,###", sym);
					
					while(scanner.hasNextLine()) {
						String line = scanner.nextLine();
						String targetText = line.split(",")[(int) spinner.getValue()-1];
						
						
						Double num = Double.parseDouble(targetText);
						
						// {"time":4.970,"data":["SpawnObj","[Up]"]},
						String finalText = txttimevaluedataspawnobjup.getText().replace("%value%", df.format(num));
						
						bw.write(finalText);
						bw.newLine();
						
						System.out.println(finalText);
					}
					
					scanner.close();
					bw.flush();
					bw.close();
					
					JOptionPane.showMessageDialog(frame, "Everything written...", "Ready", JOptionPane.INFORMATION_MESSAGE);
					
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(frame,e1.getMessage() + "\n For more information run the program in the console!","Error...",JOptionPane.ERROR_MESSAGE);
					e1.printStackTrace();
				}
				
			}
		});
		btnRun.setBounds(10, 83, 89, 23);
		frame.getContentPane().add(btnRun);
		
		spinner.setBounds(120, 83, 29, 20);
		spinner.setValue(3);
		frame.getContentPane().add(spinner);
		lblOutputFormat.setBounds(10, 130, 248, 14);
		
		frame.getContentPane().add(lblOutputFormat);
		
		txttimevaluedataspawnobjup = new JTextField();
		txttimevaluedataspawnobjup.setText("{\"time\":\"%value%\",\"data\":[\"SpawnObj\",\"[Up]\"]},");
		txttimevaluedataspawnobjup.setBounds(10, 149, 328, 20);
		frame.getContentPane().add(txttimevaluedataspawnobjup);
		txttimevaluedataspawnobjup.setColumns(10);
	}
}
